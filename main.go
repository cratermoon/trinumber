package main

import (
	"flag"
	"fmt"
)

func main() {
	var count = flag.Int("n", 23, "Number of triangle numbers to generate")
	flag.Parse()
	fmt.Printf("%d triangular numbers\n", *count)
	for n := 0; n < *count; n++ {
		fmt.Printf("%d: %d\n", n, (n * (n + 1)) / 2)
	}
}
