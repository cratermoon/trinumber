# Triangle Number Generator

Prints [triangular numbers](https://mathworld.wolfram.com/TriangularNumber.html)

## Getting Started

### Installing

`go build && go install`

### Usage

`trinumber -n 3`

`  -n int
    	Number of triangle numbers to generate (default 23)`

### Help


`trinumber -h`

## Authors

Steven E. Newton <cratermoon@gitlab.com>
